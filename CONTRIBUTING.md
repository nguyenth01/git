# **GUIDELINE CONTRIBUTING**

Tài liệu này đề cập đến một số  khái niệm, layout, công cụ nên sử dụng, convention phục vụ cho việc viết '*Markdown*'.

- [**GUIDELINE CONTRIBUTING**](#guideline-contributing)
  - [**Layout**](#layout)
  - [**Công cụ**](#c%C3%B4ng-c%E1%BB%A5)
  - [**Convention**](#convention)
    - [**Convention cho markdown**](#convention-cho-markdown)
    - [**Lưu ý thêm về số ký tự trên một dòng**](#l%C6%B0u-%C3%BD-th%C3%AAm-v%E1%BB%81-s%E1%BB%91-k%C3%BD-t%E1%BB%B1-tr%C3%AAn-m%E1%BB%99t-d%C3%B2ng)
  - [**Quy trình**](#quy-tr%C3%ACnh)
  
## **Layout**

Cấu trúc thư mục, file cơ bản được bố cụ như sau:

- [`README.md`](#README.md): File mục lục và các thông tin tổng quan của repo
- [`CONTRIBUTING.md`](#CONTRIBUTING.md): File hướng dẫn cập nhật và đóng góp thông tin của repo
- [`image`](#image):  Thư mục lưu trữ các hình ảnh liên quan đến các bài viết của repo
- [`resource`](#resource): Thư mục lưu trữ các tài liệu tham khảo : slide, pdf, ...
- [`section`](#section): Thư mục chính chứa các file markdown tri thức của repo được đề cập đến trong file [`README.md`](#README.md)
  
## **Công cụ**

Mọi người có thể  sử dụng bất kỳ editor nào mà cảm thấy thuận tiện và yêu thích nhất.
Trong repo này mình đang sử dụng:

- Editor: ***Visual Studio Code*** (vì nó miễn phí, nguồn mở, và có
nhiều extensions tốt phục vụ cho việc viết markdown).
- Extentions / Plugins:
  - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one): Chứa tất cả mọi thứ cần thiết để viết markdown (keyboard shortcuts, table of contents, auto preview và ...)
  - [markdownlint](https://marketplace.visualstudio.com/items?itemName=davidanson.vscode-markdownlint): Linting cho markdown file, Là một bộ thư viện bao gồm các quy tắc chuẩn và nhất quán phục vụ cho việc viết markdown.
  - [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=editorconfig.editorconfig): Giúp định nghĩa indent, charset, ending style, trim final new line, trim trailing whitespace tự động. Editor config được support bởi hầu hết các IDE, Editor phổ biến bây giờ.

## **Convention**

Để đảm bảo anh em viết ra một tài liệu một các đúng chuẩn thì markdown cũng
có một vài coding convention nhất định bắt buộc mọi người phải tuân theo.

### **Convention cho markdown**

Việc viết markdown được tuân thủ dựa trên rule [markdownlint
 rule](https://github.com/DavidAnson/markdownlint/blob/master/doc/Rules.md#rules).

Ví dụ khi mọi người viết sai quy chuẩn, mardkownlint sẽ báo problems cho anh em
như hình dưới.

![ ](image/markdown_problem.png)

### **Lưu ý thêm về số ký tự trên một dòng**

Giới hạn 1 dòng trong file markdown (`.md`)
< 80 ký tự để người đọc, người review và các anh em khác dễ theo dõi. Lưu ý này
chỉ dành cho file mã nguồn của doc (`.md`), không áp dụng cho file sau khi đã
được **GitLab** render ra **HTML**.

## **Quy trình**

Quy trình contributing nên được tuân thủ theo việc sử dụng tính năng ***Merge Request***
của **GitLab**.