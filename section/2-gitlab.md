# **GitLab**

- [**GitLab**](#gitlab)
  - [**Tổng quan về git và cách dùng**](#t%E1%BB%95ng-quan-v%E1%BB%81-git-v%C3%A0-c%C3%A1ch-d%C3%B9ng)
  - [**GitLab - sử dụng và các lưu ý**](#gitlab---s%E1%BB%AD-d%E1%BB%A5ng-v%C3%A0-c%C3%A1c-l%C6%B0u-%C3%BD)
    - [**Hướng dẫn sử dụng**](#h%C6%B0%E1%BB%9Bng-d%E1%BA%ABn-s%E1%BB%AD-d%E1%BB%A5ng)
    - [**Các lưu ý trên giao diện GitLab**](#c%C3%A1c-l%C6%B0u-%C3%BD-tr%C3%AAn-giao-di%E1%BB%87n-gitlab)
    - [**Khái niệm, tính năng chính nên quan tâm trong GitLab**](#kh%C3%A1i-ni%E1%BB%87m-t%C3%ADnh-n%C4%83ng-ch%C3%ADnh-n%C3%AAn-quan-t%C3%A2m-trong-gitlab)
      - [**Branch**](#branch)
      - [**Merge Request**](#merge-request)
      - [**Tag và Release**](#tag-v%C3%A0-release)
      - [**Issue list và các thao tác liên quan**](#issue-list-v%C3%A0-c%C3%A1c-thao-t%C3%A1c-li%C3%AAn-quan)

Sử dụng GitLab để lưu trữ mã nguồn của dự án, việc này sẽ giúp cho các động tác
liên quan đến CI/CD sau này trở nên dễ dàng tích hợp hơn.

## **Tổng quan về git và cách dùng**

Lưu ý: `Git`, `GitHub`, và `GitLab` là khác nhau (có thể Google để biết rõ).
Giống như `đá PES` và `máy PS` là khác nhau.

Để có thể sử dụng git cho các nhu cầu cơ bản, chỉ cần đọc qua 3 chương đầu
của cuốn `Pro Git` là đủ (xem online [tại đây](https://git-scm.com/book/en/v2)
hoặc tải [tại đây - link sách gốc](https://github.com/progit/progit2/releases/download/2.1.106/progit.pdf)
hoặc [tại đây - từ trong repo này](../resource/progit.pdf))

## **GitLab - sử dụng và các lưu ý**

### **Hướng dẫn sử dụng**

Đọc hướng dẫn sử dụng chính chủ tại đây: [http://10.240.203.2:8180/help](http://10.240.203.2:8180/help)

### **Các lưu ý trên giao diện GitLab**

Một số điểm chính cần lưu ý khi sử dụng GitLab trên nền web:

- Project
- Repository: Compare, Graph
- Issues: List, Board, Labels, Milestones
- Merge request
  - Merge request là gì
  - Tạo merge request như thế nào
  - Các thao tác có thể làm với Merge request
  - Comment/Discussion
- Wiki
- Time tracking
- Due date
- Assignee
- Project settings
- Profile settings
- Comment & Discussion

### **Khái niệm, tính năng chính nên quan tâm trong GitLab**

#### **Branch**

Nhánh trong GitLab (và Git nói chung) là một khái niệm quan trọng trong môi
trường phát triển phần mềm với sự tham gia của nhiều devs.

Tại màn hình hiển thị chính của một repository, để xem các nhánh hiện có,
ta chọn `Repository` --> `Branches`. Giao diện sẽ như dưới.

![GitLab branches](../image/gitlab-branch.png)

Danh sách các nhánh hiện có.
![GitLab branch list](../image/gitlab-branch-list.png)

#### **Merge Request**

Với DCIM, nhánh chính hiện tại là `master` (mặc định của git), mặc định, các
anh em sẽ không có quyền và không được khuyến nghị code trực tiếp trên nhánh
`master` này mà nên tuân theo quy trình có sử dụng Merge Request (đối với
GitLab, còn với GitHub thì là Pull Request, anh em lưu ý).

Để tạo Merge Request, anh em có thể tham khảo luồng dưới đây (hoặc xem docs
chính thức của Gitlab [tại đây](http://10.240.203.2:8180/help/user/project/repository/web_editor.md#tips))

*Lưu ý*: Trên giao diện GitLab có rất nhiều cách để tạo và thao tác, anh em sẽ
khám phá ra hết, tuy nhiên, ở đây chỉ giới thiệu cách "chính quy".

Bước 1: Bấm tạo mới Merge Request

![Tạo mới MR](../image/gitlab-merge-request-1.png)

Bước 2: Chọn nhánh code muốn merge và nhánh đích (thường là master)

Tại đây, Source Branch có thể là bất kỳ nhánh nào mà anh em đang phát triển,
Target Branch thường sẽ là `master` hoặc là một nhánh dùng để release (sẽ nói
trong phần CI/CD và release)

![Chọn nhánh cho MR](../image/gitlab-merge-request-2.png)

Bước 3: Nhập các nội dung liên quan đến Merge Request

- Anh em nhập tiêu đề cho Merge Request này, có thể thêm tiền tố `WIP:` kiểu
  như `WIP: Cập nhật tài liệu microservices` (WIP = Work In Progress) để chỉ
  định rằng phần công việc này đang làm, chưa cần người vào review vội.
- Anh em nhập nội dung chi tiết của Merge Request, kiểu như nhánh này làm gì,
  có những tính năng gì, fix bug gì, lưu ý gì, ...
- Assignee là người sẽ review code có trong Merge Request này của anh em.
- Ngoài ra, anh em còn có thể chọn Milestone (kiểu như nhiệm vụ tháng),
  hay Labels (thường để đánh nhãn là fix bug hay phát triển tính năng mới)
- Tùy chọn `Remove source branch when merge request is accepted.` nên được chọn
  để giúp GitLab tự xóa nhánh sau khi đã merge thành công, cái này giúp đỡ rác
  cho hệ thống, đỡ mất công anh em quản lý
- Tùy chọn `Squash commits when merge request is accepted.` sẽ gom nhiều commit
  có mặt trong Merge Request thành 1 commit, phần này sẽ làm cho history trở
  nên rất gọn gàng sạch sẽ và dễ theo dõi, tránh những commit nhỏ lẻ trông khá
  rác

![Các thông tin dành cho MR](../image/gitlab-merge-request-3.png)

Sau khi đã tạo Merge Request, anh em có code thêm gì thì chỉ việc push vào
nhánh mà anh em làm việc, các commit sẽ được hiển thị ở trên Merge Request cho
anh em quan sát và người khác review.

![Commit trong MR](../image/gitlab-merge-request-4.png)

#### **Tag và Release**

Tags trong GitLab là các nhãn (sure, và khác với label), mỗi nhãn được gắn với
1 commit nhất định trong hệ thống. Tag thường được dùng để đánh dấu vị trí của
một phiên bản phần mềm trong hệ thống.

Trong DCIM, tag được dùng để đánh dấu phiên bản release, mỗi khi tạo tag mới,
code sẽ được tự động build và đẩy lên môi trường production sử dụng phiên bản
tương ứng với tag đó.

Ví dụ: Dùng tag để đánh dấu là vị trí commit nào đó ứng với phiên bản `2.12.14`

![Danh sách các tag](../image/gitlab-tag-list.png)

#### **Issue list và các thao tác liên quan**

Quản lý danh mục các issue, có thể sử dụng issue để lưu trữ các bug,
các trao đổi công việc.

![Issue list](../image/gitlab-issue-list.png)

Board được sử dụng như bảng Kanban trong Agile

![Board](../image/gitlab-board.png)

Milestone được dùng để đặt mục tiêu trong tuần/tháng/quý này sẽ cần phải làm
những issue nào.

![Milestone](../image/gitlab-milestone.png)
