# **DOCKER**

![alt](../image/docker.png)

- [**DOCKER**](#docker)
  - [**DOCKER OVERVIEW**](#docker-overview)
    - [**Docker là gì?**](#docker-l%C3%A0-g%C3%AC)
    - [**Container vs. Virtual Machine**](#container-vs-virtual-machine)
    - [**Docker Image vs. Docker Container**](#docker-image-vs-docker-container)
    - [**Dockerfile**](#dockerfile)
  - [**DOCKER HUB**](#docker-hub)
    - [**Docker Hub**](#docker-hub)
    - [**Docker Image Command**](#docker-image-command)
    - [**Docker Container Command**](#docker-container-command)
    - [**Docker exec vs. docker run**](#docker-exec-vs-docker-run)
    - [**Docker run vs. docker start**](#docker-run-vs-docker-start)
  - [**DOCKER VOLUME**](#docker-volume)
    - [**Docker Volume**](#docker-volume)
    - [**Tại sao ta lại cần Docker Volume?**](#t%E1%BA%A1i-sao-ta-l%E1%BA%A1i-c%E1%BA%A7n-docker-volume)
    - [**Dùng Docker Volume khi nào?**](#d%C3%B9ng-docker-volume-khi-n%C3%A0o)
  - [**DOCKER COMPOSE**](#docker-compose)
    - [**Docker Compose**](#docker-compose)
    - [**Docker Compose Command**](#docker-compose-command)
  - [**DOCKER NETWORK**](#docker-network)
    - [**Docker Network**](#docker-network)
    - [**Các kiểu Docker Network mặc định**](#c%C3%A1c-ki%E1%BB%83u-docker-network-m%E1%BA%B7c-%C4%91%E1%BB%8Bnh)
  - [**DOCKER SWAM**](#docker-swam)
    - [**Docker Swam**](#docker-swam)
    - [**Docker Swarm Command**](#docker-swarm-command)
  
## **DOCKER OVERVIEW**

### **Docker là gì?**

"**`Docker`** là một dự án mã nguồn mở giúp tự động triển khai các ứng dụng
Linux và Windows vào trong các container ảo hóa." - *theo
[Wikipedia](https://vi.wikipedia.org/wiki/Docker_(ph%E1%BA%A7n_m%E1%BB%81m))*

"**`Docker`** là một nền tảng giúp cho các nhà phát triển và các nhà quản trị
`phát triển`, `triển khai` và `chạy` các ứng dụng trong `container`." -
*theo [docs.docker.com](https://docs.docker.com/get-started/)*

**Docker bao gồm các thành phần chính:**

- `Docker Engine`: dùng để tạo ra Docker image và chạy Docker container.
  
- `Docker Hub`: dịch vụ lưu trữ giúp chứa các Docker image.
  
**Một số khái niệm khác:**

- `Docker Machine`: tạo ra các Docker engine trên máy chủ.
- `Docker Compose`: chạy ứng dụng bằng cách định nghĩa cấu hình các
    Docker container thông qua tệp cấu hình
- `Docker image`: một dạng tập hợp các tệp của ứng dụng, được tạo ra bởi
    Docker engine. Nội dung của các Docker image sẽ không bị thay đổi khi
    di chuyển. Docker image được dùng để chạy các Docker container.
- `Docker Container`: một dạng runtime của các Docker image, dùng để làm
    môi trường chạy ứng dụng.

**Lợi ích của `docker`**:

- Với Docker, có thể đóng gói mọi ứng dụng vd như *webapp, backend,*
  *MySQL, BigData…* thành các `containers` và có thể chạy ở “hầu hết” các
  môi trường vd như *Linux, Mac, Window…*
- `Docker Containers` có một API cho phép quản trị các `container` từ bên ngoài.
  Giúp cho chúng ta có thể dễ dàng quản lí, thay đổi, chỉnh sửa các `container`.
- Hầu hết các ứng dụng Linux có thể chạy với `Docker Containers`.
- `Docker Containers` có tốc độ chạy nhanh hơn hẳn các `VMs` truyền thống
  (theo kiểu `Hypervisor`). Điều này là một ưu điểm nổi bật nhất của `Docker`.

### **Container vs. Virtual Machine**

![alt](../image/docker_container_vm.png)

- `Container`: Dùng chung kernel, chạy độc lập trên Host Operating System
  và có thể chạy trên bất kì hệ điều hành nào cũng như cloud.
  Khởi động và làm cho ứng dụng sẵn sàng chạy trong 500ms,
  mang lại tính khả thi cao cho những dự án cần sự mở rộng nhanh.

- `Virtual Machine`: Cần thêm một Guest OS cho nên sẽ tốn tài nguyên hơn làm
  chậm máy thật khi sử dụng. Thời gian khởi động trung bình là 20s có thể lên
  đến hàng phút, thay đổi phụ thuộc vào tốc độ của ổ đĩa.

### **Docker Image vs. Docker Container**

- `Docker Image`: Là một template chỉ cho phép đọc, ví dụ một image có thể chứa
  hệ điều hành Ubuntu và web app.Images được dùng để tạo `Docker container`.
  `Docker` cho phép build và cập nhật các image có sẵn một cách cơ bản nhất,
  hoặc bạn có thể download Docker images của người khác.
- `Docker Container`: Docker container có nét giống với các directory.
    Một `Docker container` giữ mọi thứ chúng ta cần để chạy một app.
    Mỗi `container` được tạo từ `Docker image`. Docker container có thể có các
    trạng thái ***run, started, stopped, moved, deleted.***

### **Dockerfile**

`Dockerfile` là một tập tin dạng text chứa một tập các câu lệnh để tạo mới
một Image trong Docker.

**Một số lệnh trong Dockerfile:**

- `FROM <base_image>:<phiên_bản>`: đây là câu lệnh bắt buộc phải có trong bất kỳ
  Dockerfile nào. Nó dùng để khai báo base Image mà chúng ta sẽ build mới
  Image của chúng ta.
- `MAINTAINER <tên_tác_giả>`: câu lệnh này dùng để khai báo trên tác giả tạo ra
  Image, chúng ta có thể khai báo nó hoặc không.
- `RUN <câu_lệnh>`: chúng ta sử dụng lệnh này để chạy một command cho việc
  cài đặt các công cụ cần thiết cho Image của chúng ta.
- `CMD <câu_lệnh>`: trong một Dockerfile thì chúng ta chỉ có duy nhất một
  câu lệnh CMD, câu lệnh này dùng để xác định quyền thực thi của các câu lệnh
  khi chúng ta tạo mới Image.
- `ADD <src> <dest>`: câu lệnh này dùng để copy một tập tin local hoặc remote
  nào đó (khai báo bằng `<src>`) vào một vị trí nào đó trên Container
  (khai báo bằng dest).
- `ENV <tên_biến>`: định nghĩa biến môi trường trong Container.
- `ENTRYPOINT <câu_lệnh>`: định nghĩa những command mặc định, cái mà sẽ được
  chạy khi container running.
- `VOLUME <tên_thư_mục>`: dùng để truy cập hoặc liên kết một thư mục nào đó
   trongContainer.

## **DOCKER HUB**

### **Docker Hub**

**`Docker Hub`** là dịch vụ đăng ký dựa trên đám mây cho phép bạn liên kết đến
    các kho lưu trữ mã, tạo hình ảnh(`images`) và kiểm tra chúng, lưu trữ
    hình ảnh(`images`) và liên kết đến Docker Cloud để bạn có thể triển khai
    các hình ảnh(`images`) tới máy chủ của mình.

### **Docker Image Command**

Cú pháp:

> $ docker images [OPTIONS] [REPOSITORY[:TAG]]

Options:

- `--all , -a`: Hiển thị tất cả các images (mặc định ẩn các intermediate images)
- `--digests`: Hiển thị các digests
- `--filter , -f`: Lọc dựa trên các điều kiện được cung cấp
  (dangling=(`true|false), (label=<key> or label=<key>=<value>`),
  (before=(`<image-name>[:tag]|<image-id>|image@digest`)),
  (since=(`<image-name>[:tag]|<image-id>|image@digest`)),
  (reference=(pattern of an image reference)))
- `--format`:
- `--no-trunc`:
- `--quiet , -q`: Chỉ hiển thị số ID của các images

### **Docker Container Command**

Cú pháp:

> $ docker container COMMAND

Command:

- `rename`: Đổi tên một container
- `restart`: Khởi động lại một hoặc nhiều containers
- `rm`: Loại bỏ một hoặc nhiều containers
- `run`: Chạy một lênh trong một container mới
- `start`: Khởi động một hoặc nhiều containers đã bị dừng lại trước đó
- `stop`: Dừng lại một hoặc nhiều container đang chạy
- `top`: Display the running processes of a container
- `update`: Update configuration of one or more containers
- `wait`: Block until one or more containers stop, then print their exit codes

Tham khảo thêm tại đây:
[Docker Container](https://docs.docker.com/engine/reference/commandline/container)

### **Docker exec vs. docker run**

- `docker exec`: Vận hành container docker hiện có hay đã tồn tại
  
- `docker run`: Thao tác đến các images đã tồn tại hoặc có thể truy xuất từ
  localhost, mỗi lần chạy command sẽ tạo ra một container mới tương ứng

### **Docker run vs. docker start**

- `docker run`: Thao tác đến các images đã tồn tại hoặc có thể truy xuất
    từ localhost, mỗi lần chạy command sẽ tạo ra một container mới tương ứng.
- `docker start`: Bắt đầu lại container và khởi động cho container chạy
  cho đến lần xử lý dừng tiếp theo.

## **DOCKER VOLUME**

### **Docker Volume**

![alt](../image/docker_volume_1.png)

`Volume` trong `Docker` được dùng để chia sẻ dữ liệu cho `container`.

### **Tại sao ta lại cần Docker Volume?**

- `Volumes` giản hóa việc backup hoặc migrate hơn bind mount.
    Bạn có thể quản lý `volumes` sử dụng các lệnh `Docker CLI` và `Docker API`.
- `Volumes` làm việc được trên cả Linux và Windows container.
- `Volumes` có thể an toàn hơn khi chia sẻ dữ liệu giữa nhiều container.
- `Volume drivers` cho phép bạn lưu trữ `volumes` trên remote hosts hoặc
  cloud providers, để mã hóa nội dung của `volumes`, hoặc thêm các chức năng khác.
- Các nội dung của `volume` mới có thể được điền trước bởi một `container`.

### **Dùng Docker Volume khi nào?**

- Sử dụng volume để gắn (mount) một thư mục nào đó trong host với container.
- Sử dụng volume để chia sẻ dữ liệu giữa host và container
- Sử dụng volume để chia sẽ dữ liệu giữa các container
- Backup và Restore volume.
  
## **DOCKER COMPOSE**

### **Docker Compose**

![alt](../image/docker_compose_1.png)

**`Docker Compose`** là công cụ giúp định nghĩa và khởi chạy multi-container
Docker applications.

**Docker Compose** sử dụng `Compose file` để cấu hình application's services.
Chỉ với một câu lệnh, lập trình viên có thể dễ dàng create và start toàn bộ các
services phục vụ cho việc chạy ứng dụng.

### **Docker Compose Command**

Cú pháp:

```shell
$ docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
$ docker-compose -h|--help
...
```

Một số câu lệnh thường dùng:

- Kiểm tra phiên bản docker-compose:

```shell
$ docker-compose version
docker-compose version 1.8.0, build unknown
docker-py version: 1.9.0
CPython version: 2.7.12
OpenSSL version: OpenSSL 1.0.2g  1 Mar 2016
```

- Hiển thị danh sách containers:

```shell

$ docker-compose ps
              Name                             Command               State    Ports
-----------------------------------------------------------------------------------
dockercompose01_product-service_1   python api.py                    Exit 1
dockercompose01_website_1           docker-php-entrypoint apac ...   Exit 0
docker-compose version 1.8.0, build unknown
docker-py version: 1.9.0
CPython version: 2.7.12
OpenSSL version: OpenSSL 1.0.2g  1 Mar 2016

```

- Tạo và khởi động containers:

```shell
$ docker-compose up
...
```

- Build hoặc rebuild services

```shell
$ docker-compose build
Step 1/3 : FROM python:3-onbuild
# Executing 3 build triggers
 ---> Using cache
 ---> Using cache
 ---> Using cache
 ---> 4de6e4065f4b
Step 2/3 : COPY . /usr/src/app
 ---> Using cache
 ---> edf44389cbf5
Step 3/3 : CMD ["python", "api.py"]
 ---> Using cache
 ---> a6d6dba4ebbc
Successfully built a6d6dba4ebbc
Successfully tagged dockercompose01_product-service:latest
website uses an image, skipping
```

- Xác nhận hoặc hiển thị file config Compose

```shell
$ docker-compose config
...
```

- Loại bỏ, ngừng containers:

```shell
$ docker-compose rm
Going to remove dockercompose01_website_1, dockercompose01_product-service_1
Are you sure? [yN] y
Removing dockercompose01_website_1 ... done
Removing dockercompose01_product-service_1 ... done
```

Tham khảo chi tiết tại đây:
[Docker compose](https://docs.docker.com/compose/reference/overview)
  
## **DOCKER NETWORK**

### **Docker Network**

![alt](../image/docker_network_2.png)

`Docker network` dùng để gắn địa chỉ ip cho các container thông qua một
`virtual bridge`.

### **Các kiểu Docker Network mặc định**

- `Bridge`: Network driver mặc đinh.
  `Bridge Networks` thường được sử dụng khi ứng dụng của bạn chạy trong các container
  độc lập mà chúng cần để giao tiếp.
  Tham khảo tại đây [Bridge](https://docs.docker.com/network/bridge).
- `Host`: Với các containers độc lập, xóa network cô lập giữa container
  và docker host , và sử dụng host's network trực tiếp.
  Host chỉ có trên swarm services phiên bản Docker 17.06 và cao hơn.
  Tham khảo tại đây [Host](https://docs.docker.com/network/host).
  
- `None`: Với chính container, ngắt kết nối tất cả các netwoking.
  Thường sử dụng trong kết hợp với một tùy chỉnh network driver.
  `None` cũng chỉ có trên swarm services.
  Tham khảo tại đây [None](https://docs.docker.com/network/none/).

## **DOCKER SWAM**

![alt](../image/docker_swam_1.png)

### **Docker Swam**

`Docker swarm` là một công cụ giúp chúng ta tạo ra một `clustering Docker`.
Nó giúp chúng ta gom nhiều `Docker Engine` lại với nhau và ta có thể "nhìn" nó
như duy nhất một `virtual Docker Engine`.
Khi số lượng containers tăng lên việc quản lý, triển khai trở nên phức tạp hơn
thì không thể không sử dụng `Docker Swarm`.

### **Docker Swarm Command**

- `docker swarm ca`: Hiển thị và xoay root CA
- `docker swarm init`: Khởi tạo một swarm
- `docker swarm join`: Gia nhập một swarm
- `docker swarm join-token`: Quản lý join tokens
- `docker swarm leave`: Rời khỏi swarm
- `docker swarm unlock`: Unlock swarm
- `docker swarm unlock-key`: Quản lý unlock key
- `docker swarm update`: Cập nhật swarm
  
Tham khảo chi tiết tại đây:
[Docker swarm](https://docs.docker.com/engine/reference/commandline/swarm)

***Tài liệu tham khảo:***

<https://www.docker.com/>
<https://hub.docker.com/>
<https://vi.wikipedia.org/wiki/Docker_(phần_mềm)>
