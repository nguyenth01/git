# **Git**

- [**Git**](#git)
  - [**1. Version Control System (VCS)**](#1-version-control-system-vcs)
    - [**1.1. VCS là gì?**](#11-vcs-l%C3%A0-g%C3%AC)
    - [**1.2. VCS sinh ra để làm gì?**](#12-vcs-sinh-ra-%C4%91%E1%BB%83-l%C3%A0m-g%C3%AC)
    - [**1.3. Phân loại VCS**](#13-ph%C3%A2n-lo%E1%BA%A1i-vcs)
  - [**2. Git**](#2-git)
    - [**2.1. Git là gì?**](#21-git-l%C3%A0-g%C3%AC)
    - [**2.2. Git workflow**](#22-git-workflow)
    - [**2.3. Các khái niệm cơ bản**](#23-c%C3%A1c-kh%C3%A1i-ni%E1%BB%87m-c%C6%A1-b%E1%BA%A3n)
      - [**2.3.1. Repository - Kho lưu trữ**](#231-repository---kho-l%C6%B0u-tr%E1%BB%AF)
      - [**2.3.2 Snapshot**](#232-snapshot)
      - [**2.3.3 Init**](#233-init)
      - [**2.3.4 Clone**](#234-clone)
      - [**2.3.5 Commit**](#235-commit)
      - [**2.3.6 Push**](#236-push)
      - [**2.3.7 Fetch**](#237-fetch)
      - [**2.3.8 Pull**](#238-pull)
      - [**2.3.9 Branch - Nhánh**](#239-branch---nh%C3%A1nh)

## **1. Version Control System (VCS)**

### **1.1. VCS là gì?**

***`Hệ Thống Quản lý phiên bản (Version Control System - VCS)`***
là một hệ thống lưu trữ các thay đổi của một tập tin (file) hoặc tập hợp các
tập tin theo thời gian,do đó nó giúp bạn có thể quay lại một phiên bản
xác định nào đó sau này.

### **1.2. VCS sinh ra để làm gì?**

Có hai điều quan trọng nhất để tạo ra một VCS:

- Lưu lại lịch sử các phiên bản của bất kỳ thay đổi nào của mã nguồn.
- Giúp xem lại các sự thay đổi hoặc khôi phục lại sau này.
- Chia sẻ mã nguồn trở nên dễ dàng hơn khi kết hợp với các hosting-service
  để lưu trữ các version-control. Có để public cho bất kỳ ai, hoặc private
  chỉ cho một số người có thẩm quyền có thể truy cập và lấy source về.

### **1.3. Phân loại VCS**

Đối với các hệ thống VCS, hiện tại chúng được phân ra gồm ba loại dựa trên
cách lưu trữ và chia sẻ các phiên bản:

- `Local Version Control System` - Hệ thống quản lý phiên bản cục bộ.
- `CVCS - Centralized Version Control System` - Hệ thống quản lý phiên bản tập trung.
- `DVCS - Distributed Version Control System` - Hệ thống quản lý phiên bản phân tán.

Trong tài liệu này mình sẽ không đề cập nhiều đến vấn đề này.
Mọi người vui lòng tham khảo tại đây [About Version Control](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)

## **2. Git**

### **2.1. Git là gì?**

***`Git`*** là một hệ thống quản lý phiên bản phân tán
(Distributed Version Control System – DVCS)
ra đời vào năm 2005 và hiện được dùng rất phổ biến. So với các
hệ thống quản lý phiên bản tập trung khi tất cả mã nguồn và lịch sử  thay đổi
chỉ được lưu một nơi là máy chủ thì trong hệ thống phân tán, các máy khách không
chỉ "check out" phiên bản mới nhất của các tập tin mà là sao chép (mirror)
toàn bộ kho mã nguồn (repository). Như vậy, nếu như máy chủ ngừng hoạt động,
thì bạn hoàn toàn có thể lấy kho chứa từ bất kỳ máy khách nào để sao chép ngược
trở lại máy chủ để khôi phục lại toàn bộ hệ thống. Mỗi checkout thực sự là một
bản sao đầy đủ của tất cả dữ liệu của kho chứa từ máy chủ.

![Mô hình tổ chức dữ liệu phân tán trên Git](../image/git_distribute_structure.png)

### **2.2. Git workflow**

Mỗi tập tin trong Git được quản lý dựa trên ba trạng thái: `committed`,
`modified`, và `staged`.

- `Committed`: có nghĩa là dữ liệu đã được lưu trữ một cách an toàn
  trong cơ sở dữ liệu.
- `Modified`: có nghĩa là bạn đã thay đổi tập tin nhưng chưa commit
  vào cơ sở dữ liệu.
- `Staged`: là bạn đã đánh dấu sẽ commit phiên bản hiện tại của một tập tin
  đã chỉnh sửa trong lần commit sắp tới.

![ ](../image/git_local_operations.png)

- `Git directory`: là nơi Git lưu trữ các "siêu dữ kiện" (metadata)
  và cơ sở dữ liệu cho dự án của bạn.
- `Working directory`: là bản sao một phiên bản của dự án.
- `Staging area`: là một tập tin đơn giản được chứa trong thư mục Git,
  nó chứa thông tin về những gì sẽ được commit trong lần commit sắp tới.

Tiến trình công việc (workflow) cơ bản của Git:

1. Bạn thay đổi các tập tin trong thư mục làm việc (`Working directory`).
2. Bạn tổ chức các tập tin, tạo mới ảnh của các tập tin đó vào
   khu vực tổ chức (`Staging area`).
3. Bạn commit, ảnh của các tập tin trong khu vực tổ chức (`Staging area`)
   sẽ được lưu trữ vĩnh viễn vào thư mục Git (`Git directory`).

### **2.3. Các khái niệm cơ bản**

#### **2.3.1. Repository - Kho lưu trữ**

Trong Git, `Repository` là nơi lưu trữ, quản lý tất cả những thông tin
cần thiết (thư mục, tập tin, ảnh, video, bảng biểu, dữ liệu… ) cũng như các sửa
đổi và lịch sử của toàn bộ dự án.
Có hai loại repository, đó là local repository và remote repository.

![ ](../image/git_repository.png)

#### **2.3.2 Snapshot**

Cơ chế lưu trữ phiên bản của Git là sau mỗi lần bạn thực hiện lưu trạng thái
(commit) sẽ tạo ra một "`ảnh chụp`" (`snapshot`) lưu lại nội dung tất cả các tập
tin, thư mục tại thời điểm đó rồi tạo tham chiếu tới snapshot đó.
Để hiệu quả hơn, nếu như tập tin không có thay đổi, Git không lưu trữ tập tin đó
lại mà chỉ tạo liên kết tới tập tin gốc đã tồn tại trước đó. Sau đó khi cần bạn
hoàn toàn có thể khôi phục và sử dụng lại một snapshot, hay còn gọi là phiên bản
nào đó. Đây cũng chính là lợi thế của Git khi nó không lưu dữ liệu mà sẽ lưu
dạng snapshot, giúp tiết kiệm không gian lưu trữ.

![ ](../image/git_snapshot.png)

#### **2.3.3 Init**

 Lệnh này dùng để khởi tạo một repository.
 Lệnh này sẽ tạo một thư mục mới có tên `.git`, thư mục này chứa tất cả các tập
 tin cần thiết cho kho chứa - đó chính là bộ khung/xương của kho chứa Git.

#### **2.3.4 Clone**

Nếu bạn muốn có một bản sao của một kho chứa Git có sẵn, có thể là một dự án mà
bạn tham gia – thì bạn hãy thực hiện clone. Đây là điểm khác biệt của80 Git so
với một số hệ thống quản lý phiên bản mã nguồn khác vì clone là tạo ra một bản
sao của gần như tất cả những gì của repository mà máy chủ đang lưu trữ.
Bạn sẽ có được tất cả lịch sử đã xảy ra trên repository và hoàn toàn có thể quay
lại, undo lại từ bất kỳ thời điểm commit nào. Và một điểm nữa là nếu ổ cứng máy
chủ bị hư, bạn có thể sử dụng bất kỳ bản sao trên bất kỳ máy khách nào để khôi
phục lại trạng thái của máy chủ.

![ ](../image/git_clone.png)

#### **2.3.5 Commit**

`Commit` là thao tác báo cho hệ thống biết bạn muốn lưu lại trạng thái hiện hành,
ghi nhận lại lịch sử các xử lý như đã thêm, xóa, cập nhật các file hay thư mục
nào đó trên repository.

Khi thực hiện commit, trong repository sẽ ghi lại sự khác biệt từ lần commit
trước với trạng thái hiện tại. Các commit ghi nối tiếp với nhau theo thứ tự
thời gian do đó chỉ cần theo vết các commit thì có thể biết được lịch sử
thay đổi trong quá khứ.

![ ](../image/git_commit.png)

#### **2.3.6 Push**

`Push` là cách bạn chuyển giao các commit từ kho lưu trữ cục bộ của bạn lên
server. Lệnh `push được` sử dụng để đưa nội dung kho lưu trữ cục bộ lên server.

![ ](../image/git_push.png)

#### **2.3.7 Fetch**

Khi ta thực hiện `fetch`, lệnh này sẽ truy cập vào repository trên server và kéo
toàn bộ dữ liệu mà bạn chưa có từ repository trên server về. Sau đó, bạn có thể
tích hợp dữ liệu vào branch bất kỳ lúc nào.

#### **2.3.8 Pull**

Lệnh này sẽ tự động lấy toàn bộ dữ liệu từ repository trên server và gộp vào cái
branch hiện tại bạn đang làm việc.

#### **2.3.9 Branch - Nhánh**

`Branch` là nhánh của repository: Các nhánh sẽ độc lập với nhau và phát triển
một tính năng, không gây ảnh hưởng đến các nhánh khác. Khi các nhánh hợp nhất
lại với nhau thì gọi là merge, nhánh mặc định là master. Branch ở trên
local repo thì gọi là local branch. Branch ở trên remote repo thì gọi là
remote branch. Một branch trên local có thể liên kết với 1 hoặc nhiều branch
trên remote hoặc không branch nào cả.

Nhánh là khái niệm rất hay trong Git, với nhánh bạn có thể tách riêng các
tính năng của dự án, thử nghiệm các tính năng mới hay cũng có thể dùng nhánh để
khắc phục, hoàn chỉnh lỗi nào đó của dự án,…

Khi bắt đầu khởi tạo một repository hoặc clone một repository, bạn sẽ có một
nhánh (branch) chính tên là master, đây là branch chứa toàn bộ các mã nguồn chính
trong repository. Từ nhánh master này, trong quá trình thực hiện dự án bạn có
thể rẽ thêm nhiều nhánh khác tùy theo nhu cầu thực tế. Tất cả các nhánh đều được
hệ thống lưu lại lịch sử các lần commit trên nhánh và bạn hoàn toàn quay lại mốc
commit nào mà mình muốn.

![ ](../image/git_branch_01.png)

Một lưu ý nhỏ chỗ này là tất cả thao tác fetch, push hoặc pull mặc định
thực hiện trên nhánh hiện hành, nên bạn lưu ý nhánh mình đang thao tác là
nhánh master hay nhánh nào để tránh sai sót.

![ ](../image/git_branch_02.png)

Bạn quyết định sẽ nâng cấp dự án qua việc giải quyết vấn đề số #53 *(cách đặt mã
số vấn đề theo quy ước của dự án)* nên bạn sẽ tạo một nhánh mới **iss53** để cho
lần nâng cấp đó, bạn sẽ giải quyết vấn đề #53. Bạn sẽ tạo nhánh mới **iss53**,
vậy là từ lần commit C2 trong repository của bạn sẽ có 2 nhánh là **master**
và **iss53**.

![ ](../image/git_branch_03.png)

Bạn tiếp tục làm việc trên nhánh **iss53** và sau đó thực hiện commit ở C3.

![ ](../image/git_branch_04.png)

Bây giờ bạn nhận được thông báo rằng có một vấn đề với dự án và bạn cần khắc phục
nó ngay lập tức. Bạn có vấn đề mình đang giải quyết **(iss53)** nhưng chưa xong
và lỗi mới phát hiện, cần khắc phục. Với Git, bạn không cần phải tốn quá nhiều
công sức để khôi phục lại các thay đổi trước khi bạn làm **iss53** để vá lỗi rồi
làm tiếp **iss53**. Hoàn toàn độc lập, tất cả những gì bạn cần phải làm là
chuyển lại nhánh **master** và lấy lại mã lệnh ở thời điểm mình cần.

Tuy nhiên, trước khi làm điều này, bạn nên lưu ý rằng nếu thư mục làm việc hoặc
khu vực tổ chức có chứa các thay đổi chưa được commit mà xung đột với nhánh bạn
đang làm việc, Git sẽ không cho phép bạn chuyển nhánh. Tốt nhất là bạn nên ở
trạng thái làm việc "sạch" (đã commit hết) trước khi chuyển nhánh.
Bạn sẽ quay lại thời điểm commit mình cần, giả sử C2, lúc này thư mục làm việc
của dự án giống hệt như trước khi bạn bắt đầu giải quyết vấn đề #53 và bạn có
thể tập trung vào việc sửa lỗi. Điểm quan trọng cần ghi nhớ ở đây là Git sẽ khôi
phục lại thư mục làm việc của bạn giống như snapshot của lần commit C2.
Git sẽ tự động thêm, xóa và sửa các tập tin sao cho đảm bảo rằng thư mục làm
việc của bạn giống như lần commit C2.

Để sửa lỗi, bạn tạo nhánh mới **hotfix** dựa trên nhánh master để giải quyết
các lỗi mới phát sinh và bạn giải quyết xong vấn đề với lần commit C4.

![ ](../image/git_branch_05.png)

Bạn có thể chạy để kiểm tra và chắc chắn rằng bản vá lỗi hoạt động đúng theo ý
bạn muốn rồi sau đó tích hợp nó lại nhánh chính **master** để triển khai với
thao tác **merge**. Để merge, bạn sẽ chuyển sang nhánh **master** trước rồi thực
hiện merge nhánh **master** với nhánh **hotfix**. Sau khi merge những cập nhật
mới của bạn bây giờ ở trong snapshot của commit C4 được trỏ tới bởi nhánh
**master** và bạn có thể mang đi triển khai thực tế phiên bản này.

![ ](../image/git_branch_06.png)

Vậy là xong, lúc này bạn cần xóa nhánh **hotfix** đi vì bạn không còn cần tới
nó nữa, nhánh **master** đã trỏ đến lần commit bạn cần là C4. Bạn đã triển khai
xong bản vá lỗi quan trọng và sẵn sàng quay lại với công việc bị gián đoạn trước
đó. Bạn sẽ chuyển sang nhánh **iss53** mà bạn đang làm việc trước đó để tiếp tục
giải quyết vấn đề #53.

![ ](../image/git_branch_07.png)

Như vậy với Git, bạn hoàn toàn chủ động trong các giai đoạn của dự án mà không
làm ảnh hưởng đến tiến độ chung cũng như công việc đang thực hiện của người khác.
Ngoài cách dùng merge để tích hợp những thay đổi từ một nhánh vào một nhánh khác
bạn còn có thể sử dụng **`rebase`**.

`Rebase`** cũng là cách để bạn tích hợp các thay đổi từ một nhánh này sang nhánh
khác. Nhưng so với merge là gộp nhánh, tức là lấy snapshot mới nhất của mỗi
branch rồi kết hợp lại với nhau. Như vậy, mỗi khi bạn merge một feature branch
về master branch thì đơn giản là nó sẽ tạo ra một merge commit ở master branch.
Trong khi đó nếu chúng ta sử dụng rebase, ví dụ như cần tích hợp feature branch
vào nhánh master thì nó sẽ đem tất cả các thay đổi, các commit từ nhánh feature
vào nhánh branch hay nói cách khác là nó sẽ sao chép tất cả các thay đổi từ
nhánh feature đặt lên trước nhánh master lần lượt theo thứ tự.

![ ](../image/git_branch_08.png)

Vậy thì khi nào bạn dùng rebase và khi nào bạn dùng merge? Rõ ràng bạn sẽ thấy
lịch sử trên git của merge nhìn rối hơn, đặc biệt là khi bạn có nhiều nhánh con
trong khi rebase thì bạn cho một lịch sử dễ nhìn hơn. Tuy nhiên, merge là cách
gộp nhánh an toàn, đặc biệt khi có nhiều người tham gia cùng dự án. Nó sẽ
lưu lại thật sự tất cả những gì đã diễn ra trong quá trình thực hiện dự án.
Trong khi đó,nếu bạn sử dụng rebase chủ quan, không đúng cách thì có thể mất
commit trên nhánh của người khác.
