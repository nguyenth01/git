# **Git-flow**

## **Khái quát về `git-flow`**

**`Git-flow`** là tên gọi của 1 tool (command) hỗ trợ branch model gọi là
    A successful Git branching model do Vincent Driessen đề xuất ra.
    Người ta thường gọi đó là model hay tool, và trong git-flow có 5 kiểu với
    mỗi vai trò khác nhau (tùy trường hợp mà có lúc là 6 kiểu), switch các kiểu
    với nhau để phát triển Bằng việc set trước các branch, những rule khi merge,
    dù có bao nhiêu developer cùng thời điểm phát triển vẫn có thể quản lý
    branch dễ dàng, và tránh được những vấn đề do merge Tiếp theo, chúng ta cùng
    xem về các kiểu của branch có trong git-flow

## **Branch trong `git-flow`**

Trước khi vào chi tiết chúng ta xem qua về định nghĩa các kiểu branch trong
git-flow trước nhé.

### **Master brach**

Branch master là branch được tạo mới repository và tạo mặc định trong Git.
Những người mới bắt đầu thường có xu hướng commit trực tiếp và branch master,
nhưng trong git-flow thì ta không commit trực tiếp vào master, mà đây chỉ là
branch dùng để thực hiện merge, nên chúng ta lưu ý nhé.

### **Develop branch**

Branch develop là branch trung tâm cho việc phát triển. Do với mỗi thay đổi
ta lại ngắt branch feature tương ứng cho nên có thể nói đây là branch được dùng
nhiều nhất trong quá trình phát triển. Cần đặt tên branch sao cho người khác có
thể biết được ngay nội dung thay đổi là gì. Mỗi branch được ngắt ra để làm này
sau khi làm xong ta lại merge vào develop, merge xong sẽ xóa nó đi.

### **Release branch**

Branch release là branch dùng để release sản phẩm như đúng tên gọi của nó.
Khi release sản phẩm thì có rất nhiều những task liên quan khác cần thiết nữa,
những task liên quan đó sẽ được release trên branch release mà ta ngắt ra từ
branch develop. Sau khi release xong, sẽ merge vào branch master và
branch develop, ghi release tag vào merge commit của branch master sau đó
xóa branch release đi.

### **Hotfix branch**

Khi release sản phẩm cũng có khi ta phát hiện ra bug rất nghiêm trọng, chắc hẳn
mọi người cũng từng trải qua vấn đề này rồi nhỉ? Những lúc như vậy ta sẽ ngắt ra
branch hotfix trực tiếp từ branch master để tiến hành sửa, sau khi sửa xong sẽ
merge vào master và develop và ghi lại release tag. Sau đó sẽ xóa branch hotfix 
đi.

### **Support branch**

## **Tổng hợp**

Tổng thể flow của git-flow như sau:

1. Một `Develope branch` sẽ được tạo từ `Master branch`.
2. Một `Release branch` được tạo từ `Develope branch`.
3. Khi một `feature` hoàn thành nó sẽ được gộp (`merged`)
   vào với `Develope branch`.
4. Khi một `Release branch` hoàn thành nó sẽ đươc gộp (`merged`)
   vào cả 2 `Develope branch` và `Master branch`.
5. Nếu có một vấn đề (`bugs`) được phát hiện trong `Master branch`
   thì một `Hotfix branch` sẽ được khởi tạo từ `Master branch`.
6. Khi `Hotfix branch` hoàn thành nó sẽ được gộp (`merged`)
   vào cả 2 `Develope branch` và `Master branch`.


***Tài liệu tham khảo:***

[https://viblo.asia/p/git-flow-maGK7W0AKj2]
(https://viblo.asia/p/git-flow-maGK7W0AKj2)

[https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow]
(https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)