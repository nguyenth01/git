# **PIP**

- [**PIP**](#pip)
  - [**Pip - Overview**](#pip---overview)
  - [**PIP - Installation**](#pip---installation)
  - [**PIP - Update**](#pip---update)
  - [**PIP - Commands**](#pip---commands)
    - [**Pip - Search package**](#pip---search-package)
    - [**Pip - Install package**](#pip---install-package)
    - [**Pip - Find Packages**](#pip---find-packages)
    - [**Pip - List package**](#pip---list-package)
    - [**Pip - Show package information**](#pip---show-package-information)
    - [**Pip - Uninstall package**](#pip---uninstall-package)
  - [**PIP - Configs**](#pip---configs)
    - [**Pip - Configs proxy**](#pip---configs-proxy)
    - [**Requirements Files**](#requirements-files)

## **Pip - Overview**

**`Pip`** là một hệ thống quản lý gói (`package management system`)
được sử dụng để cài đặt và quản lý các gói phần mềm được viết bằng `python`.
Nhiều gói có thể được tìm thấy trong nguồn mặc định cho các gói và phần phụ
thuộc của chúng - `Python Package Index`(pypi).

## **PIP - Installation**

Đối với `python >=2.7.9` và `python >=3.4` **`pip`** được bao gồm mặc định.

Để cài đặt pip, download file [get-pip.py](https://bootstrap.pypa.io/get-pip.py)

```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```

Sau đó chạy lệnh:

```bash
python get-pip.py
```

- **Trên Centos/RHEL**

Vì PIP và wheel không có trên repo mặc định của Centos nên chúng ta cần
cài đặt EPEL repo trước.

```bash
sudo yum install epel-release
```

Sau đó cài đặt PIP và wheel

```bash
sudo yum install python-pip
```

```bash
sudo yum install python-wheel
```

Tiếp theo:

```bash
sudo yum upgrade python-setuptools
```

- **Trên Debian/Ubuntu/Linux Mint**

  - Cài đặt Python2:

    ```bash
    sudo apt-get install python-pip
    ```

  - Cài đặt python3

    ```bash
    sudo apt-get install python3-pip
    ```

Với phiên bản Ubuntu cũ 12.04, python3 không có mặc định vì vậy ta phải dùng
lệnh:

```bash
sudo apt-get install python3-setuptools
sudo easy_install3 pip
```

## **PIP - Update**

On Linux or macOS:

```bash
pip install -U pip
```

On Windows:

```bash
python -m pip install -U pip
```

## **PIP - Commands**

```bash
Usage:
  pip  [options]

Commands:
  install       Install packages.
  uninstall     Uninstall packages.
  freeze        Output installed packages in requirements format.
  list          List installed packages.
  show          Show information about installed packages.
  search        Search PyPI for packages.
  zip           Zip individual packages.
  unzip         Unzip individual packages.
  bundle        Create pybundles.
  help          Show help for commands.
```

### **Pip - Search package**

```shell
pip search  <package-name>
```

### **Pip - Install package**

```shell
pip install <package-name>
```

### **Pip - Find Packages**

Để tìm kiếm package, truy cập vào link: [https://pypi.org](https://pypi.org).

### **Pip - List package**

```shell
pip list
```

### **Pip - Show package information**

```shell
pip show <package-name>
```

### **Pip - Uninstall package**

Sử dụng lệnh gỡ cài đặt để xóa gói:

```shell
pip uninstall <package-name>
```

## **PIP - Configs**

- **Per-user**:

  - On Unix the default configuration file is: `$HOME/.config/pip/pip.conf`
  - On macOS the configuration file is `$HOME/Library/Application Support/pip/pip.conf`
    if directory `$HOME/Library/Application Support/pip` exists
  else `$HOME/.config/pip/pip.conf`.
  - On Windows the configuration file is `%APPDATA%\pip\pip.ini`

- **Inside a virtualenv**:

  - On Unix and macOS the file is `$VIRTUAL_ENV/pip.conf`
  - On Windows the file is: `%VIRTUAL_ENV%\pip.ini`

### **Pip - Configs proxy**

```shell
pip install <package> --proxy=<proxy server and port>
```

### **Requirements Files**

**`Requirements Files`** là file chứa danh sách các mục sẽ được cài đặt bằng
cách sử dụng `pip`.

```shell
pip install -r requirements.txt
```

Mỗi dòng của **`Requirements Files`** chỉ ra một package sẽ được cài đặt và
giống như các đối số truyền vào pip.

- `Requirements files` được sử dụng để lưu giữ lại kết quả cài đặt bằng pip nhắm
  mục đích sử dụng cho những cài đặt sau. Trong trường hợp này
  `Requirements files` chứa mọi đã được cài đặt của phiên bản trước đó sau
  khi `pip freeze` được thực thi:

```shell
pip freeze > requirements.txt
```

- `Requirements files` được sử dụng để buộc pip giải quyết đúng các dependencies.
- `Requirements files`được sử dụng để buộc pip cài đặt phiên bản thay thế của
  dependencies. (sub-dependency)
- `Requirements files` được sử dụng để ghi đè một dependency với một bản vá
  cục bộ nằm trong kiểm soát phiên bản.
